import { RollPF } from "./roll.js";
import { sizeDieExt } from "./lib.js";

/**
 * RollTerm for sizeRoll() function
 */
export class SizeRollTerm extends RollTerm {
  constructor({ terms = [], roll, options }) {
    super({ options });

    if (terms) {
      if (terms[0] instanceof RollTerm) this.terms = terms;
      else if (typeof terms[0] === "string") {
        this._terms = terms;
        this.terms = terms.reduce((all, t) => {
          const ts = RollPF.parse(t);
          if (ts.length > 1) all.push(ParentheticalTerm.fromTerms(ts));
          else all.push(ts[0]);
          return all;
        }, []);
      } else this.terms = terms.map((t) => RollTerm.fromData(t));
    }

    this.roll = roll ? (roll instanceof Roll ? roll : RollPF.fromData(roll)) : undefined;
  }

  _terms = [];

  terms = [];

  roll = undefined;

  isIntermediate = false;

  static SERIALIZE_ATTRIBUTES = ["terms", "roll"];
  static MODIFIERS = {};

  get total() {
    return this.roll.total;
  }

  get dice() {
    return this.roll?.dice;
  }

  get formula() {
    return this.simplify ?? super.formula;
  }

  get expression() {
    return `sizeRoll(${this.terms.map((t) => t.formula).join(", ")})`;
  }

  /**
   * Return simpler representation of the sizeroll (3d6 instead of sizeroll(3, 6)).
   */
  get simplify() {
    let f = this.roll?.formula;
    if (f && this.flavor) f += `[${this.flavor}]`;
    return f;
  }

  get isDeterministic() {
    return false;
  }

  _evaluateSync({ minimize = false, maximize = false } = {}) {
    throw new Error("SizeRollTerm does not support synchronous evaluation");
  }

  async _evaluate({ minimize = false, maximize = false } = {}) {
    for (const term of this.terms) {
      if (term._evaluated) continue;
      await term.evaluate({ minimize, maximize, async: true });
    }

    const noRoll = !this.roll;
    const sizeDice = noRoll ? sizeDieExt(...this.terms.map((r) => r.total)) : null;
    if (sizeDice && this.flavor) sizeDice[0].options.flavor = this.flavor;
    const roll = noRoll ? RollPF.fromTerms(sizeDice) : this.roll;
    if (this.flavor) roll.options.flavor = this.flavor;

    this.roll = roll._evaluated ? roll : await roll.evaluate({ minimize, maximize, async: true });

    return this;
  }
}
